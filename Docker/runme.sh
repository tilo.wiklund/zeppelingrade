#!/bin/bash

#/zeppelin/bin/zeppelin-daemon.sh start 2> /zeppelin.log > /zeppelin.out &

python /wait_up.py
if [ $? -ne 0 ]; then
    echo Failed to access Zeppelin
    exit 1
fi

for f in /sources/*; do
    python /runme.py $f /tests/*.json
    if [ $? -ne 0 ]; then
        exit 1
    fi
done
