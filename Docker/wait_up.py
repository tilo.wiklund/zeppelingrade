import argparse
import requests
import time
import os

parser = argparse.ArgumentParser(description='Run tests on Scala source')
parser.add_argument('-a', '--host', default='localhost', dest='host')
parser.add_argument('-p', '--port', default='8080', dest='port')
parser.add_argument('-t', '--timeout', default='60', dest='timeout', type=float)
args = parser.parse_args()

host = "http://{}:{}".format(args.host, args.port)

def check_available():
    requestURL  = "{}/api/notebook".format(host)
    try:
        r = requests.get(requestURL)
        return r.json()["status"] == "OK"
    except Exception:
        return False

start = os.times()[4]
ellapsed = 0
while (not check_available()) and ellapsed < args.timeout:
    time.sleep(1)
    ellapsed = os.times()[4] - start
    # print(ellapsed)

if ellapsed >= args.timeout:
    exit(1)

